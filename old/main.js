var fs = require('fs');
var ip = require('ip');
var Steam = require('steam');

// Load the users
if (!fs.existsSync('settings.json')) {
	console.log('Missing settings.json file');
	return;
}

var welcomeText = "Hello! I'm a bot designed for one thing: notifying people. If you do not with to receive my notifications, please unfriend me."
var settings = JSON.parse(fs.readFileSync('settings.json'));

function convertToBool(string) {
	switch(string.toLowerCase()) {
		case "true": case "t": case "yes": case "y": case "1": return true;
		case "false": case "f": case "no": case "n": case "0": return false;
		default: return Boolean(string);
	}
}

function saveSettings() {
	fs.writeFile('settings.json', JSON.stringify(settings, null, '\t'));
}

function getUser(sid) {
	return settings.users[sid] || {};
}

function isSeeder(sid) {
	return getUser(sid).seeder || false;
}

function isAdmin(sid) {
	return getUser(sid).admin || false;
}

var props = ['notify', 'nick'];
var boolProps = ['notify', 'admin'];
var adminProps = ['admin', 'notify', 'seeder', 'nick', 'internal_nick'];

// if we've saved a server list, use it
if (settings.servers != undefined) {
	Steam.servers = settings.servers;
}

var bot = new Steam.SteamClient();
bot.logOn({
	accountName: settings.accountName,
	password: settings.password
});

bot.on('error', function(err) {
	console.log(err);
	process.exit(1);
});

bot.on('user', function(user) {
	if (user.personaState == Steam.EPersonaState.Online) {
		console.log(user.playerName + ' just came online.');
	} else if (user.personaState == Steam.EPersonaState.Offline) {
		console.log(user.playerName + ' just logged off.');
	}
	console.log(user);
});

bot.on('friend', function(sid, friendStatus) {
	// This handles any invites that happened while we are online
	if (friendStatus == Steam.EFriendRelationship.PendingInvitee) {
		bot.addFriend(sid);
		bot.sendMessage(sid, welcomeText);
	}
});

bot.on('relationships', function() {
	// Loop through 'friends' and accept any invites
	// This handles any invites that happened while we were offline
	for (var sid in bot.friends) {
		var friendStatus = bot.friends[sid];
		if (friendStatus == Steam.EFriendRelationship.PendingInvitee) {
			bot.addFriend(sid);
			bot.sendMessage(sid, welcomeText);
		}
	}
});

bot.on('loggedOn', function() {
	console.log('Logged in!');
	bot.setPersonaState(Steam.EPersonaState.Online); // to display your bot's status as "Online"
	bot.setPersonaName(settings.displayName); // to change its nickname
});

bot.on('servers', function(servers) {
	settings.servers = servers;
	saveSettings();
});

bot.on('message', function(source, message, type, chatter) {
	if (type != Steam.EChatEntryType.ChatMsg) {
		// We only respond to chat messages
		return
	}

	msg = message.split(" ");
	cmd = msg.shift();

	if (cmd == "!seed") {
		if (!isSeeder(source)) {
			bot.sendMessage(source, "You don't have permission to do that! Please contact the admins if you feel you should.");
			return;
		}

		var serverIp = settings.gameServers[msg[0]];
		if (serverIp != undefined) {
			msg.shift();
		}
		var from = "";
		var sender = bot.users[source];
		var text = msg.join(' ');
		var count = 0;
		for (var sid in bot.friends) {
			var friendStatus = bot.friends[sid];
			var user = bot.users[sid];
			if (friendStatus != Steam.EFriendRelationship.Friend) {
				// Only message friends
				continue;
			} else if (sid == source) {
				// Don't send to the original user
				continue;
			} else if (sid == bot.steamID) {
				// Don't message ourself
				continue;
			} else if (user.personaState != Steam.EPersonaState.Online) {
				// Only send if we can see they're online
				console.log('Excluding ' + user.playerName + ' because not online: ' + user.personaState);
				continue;
			} else if (user.gamePlayedAppId != 440 && user.gamePlayedAppId != 0) {
				// Only send if they're not in a game, or playing TF2
				console.log('Excluding ' + user.playerName + ' because game status');
				continue;
			} else if (serverIp != undefined && serverIp == ip.fromLong(user.gameServerIp)) {
				// Only send if they're not on the requested server.
				console.log('Excluding ' + user.playerName + ' because server ip');
				continue;
			}

			bot.sendMessage(source, "Sending message to " + user.playerName);
			if (sender != undefined && sender.playerName != undefined) {
				bot.sendMessage(sid, sender.playerName + ": " + text);
			} else {
				bot.sendMessage(sid, text);
			}
			count++;
		}

		bot.sendMessage(source, "" + count + " messages sent.")
	} else if (cmd == "!set" || cmd == "!setuser") {
		sid = source;
		if (cmd == "!setuser") {
			if (!isAdmin(source)) {
				bot.sendMessage(source, "You don't have permission to do that!");
				return;
			}
			sid = msg.shift();
		}
		prop = msg.shift();
		value = msg.join(' ');

		if (prop == undefined || sid == undefined || value == undefined) {
			if (cmd == "!setuser") {
				bot.sendMessage(source, 'Usage: !setuser steamid property value');
			} else {
				bot.sendMessage(source, 'Usage: !set property value');
			}
			return
		}

		if (isAdmin(source)) {
			// If the user is an admin, check against the admin list
			if (adminProps.indexOf(prop) < 0) {
				bot.sendMessage(source, prop + ' is not a valid property. Valid properties: ' + adminProps.join(", "));
				return;
			}
		} else {
			// Otherwise, use the public list
			if (props.indexOf(prop) < 0) {
				bot.sendMessage(source, prop + ' is not a valid property. Valid properties: ' + props.join(", "));
				return;
			}
		}

		// If it's a bool prop, convert it into one
		if (boolProps.indexOf(prop) > -1) {
			value = convertToBool(value);
		}

		console.log('Setting ' + prop + ' to ' + value + ' for id ' + sid);
		if (settings.users[sid] == undefined) {
			settings.users[sid] = {}
		}

		// Set the property
		settings.users[sid][prop] = value;

		// Save the file
		saveSettings();
	} else {
		bot.sendMessage(source, welcomeText);
	}
});
