var fs = require('fs');
var ip = require('ip');
var Steam = require('steam');

// Local packages
var user = require('user');

var levels = {
	'admin': 2,
	'user': 1
};

// Possible props and settings
var props = {
	'admin': {
		'type': 'admin',
		'default': false,
		'save': saveBool
	},
	'seeder': {
		'type': 'admin',
		'default': false,
		'save': saveBool
	},
	'notify_game_blacklist': {
		'type': 'user',
		'default': ['*'],
		'save': saveList,
		'display': displayList
	},
	'notify_game_whitelist': {
		'type': 'user',
		'defaultValue': ['*'],
		'save': saveList,
		'display': displayList
	},
	'notify_game_type': {
		'type': 'user',
		'defaultValue': 'blacklist',
		'save': saveOptions
	},
	'notify': {
		'type': 'user',
		'defaultValue': ['online', 'ingame'],
		'save': saveList,
		'display': displayList
	}
}

var propsFor = function(type) {
	var plist = [];
	for (var prop in props) {
		if (levels[props[prop].type] <= levels[type]) {
			plist.append(prop);
		}
	}
};

var saveList = function(val) {
	return val.split(',');
};

var displayList = function(val) {
	return val.join(',');
};

var cmdCheck = /^!([a-z]+)(?:\s(.+))?$/

// Load settings
if (!fs.existsSync('settings.json')) {
	console.log('Missing settings.json file');
	process.exit(1);
	return;
}

var settings = JSON.parse(fs.readFileSync('settings.json'));

// Load in users
var users = {};
if (fs.existsSync('users.json')) {
	users = JSON.parse(fs.readFileSync('users.json'));
}

// Load the server cache
var cache = {};
if (fs.existsSync('cache.json')) {
	cache = JSON.parse(fs.readFileSync('cache.json'));
	if (cache.users == undefined) {
		cache.users = {};
	}
	Steam.servers = cache.servers;
}

var saveBool = function(string) {
	switch(string.toLowerCase()) {
		case "true": case "t": case "yes": case "y": case "1":
			return true;
		case "false": case "f": case "no": case "n": case "0":
			return false;
		default:
			throw string + 'is not a valid value. Try true or false.'
	}
}

var splitText = function(text) {
	if (text == undefined) {
		return [];
	}

	return text.split(' ');
}

var steam2Check = /^STEAM_[0-5]:([0-1]):([0-9]+)$/;
var steam3Check = /^\[U:1:([0-9]+)\]$/;
var communityCheck = /^7656119([0-9]{10})$/;

var getCommunityId = function(sid) {
	if (sid.match(communityCheck) != null) {
		return sid;
	}

	var accountId = null;
	var match = steam2Check.match(sid);
	if (match != null) {
		accountID = (match[2] << 1) | match[1];
		return '7656119' + (7960265728 + accountID);
	} else {
		var match = steam3Check.match(sid);
		if (match != null) {
			accountID = parseInt(match[1]);
			return '7656119' + (7960265728 + accountID);
		}
	}

	// TODO: Fall back to profile url
	return null;
}

var User = function(sid) {
	// sid conversion from https://github.com/xPaw/SteamID/blob/master/SteamID.js

	// Check which type of SID we're getting.
	// Note that there are 3 types:
	// * Steam Community ID (what we need)
	// * Steam ID
	// * Custom URL
	this.sid = getCommunityId(sid);
	if (sid == null) {
		return null;
	}

	var user = users[this.sid] || {};
	this.props = user || {};
	this.notify = this.get('notify');
	this.friendStatus = bot.friends[this.sid] || Steam.EFriendRelationship.None;

	// Possibly cached properties
	this.displayName = undefined;
	this.personaState = undefined;
	if (cache.users[this.sid] != undefined) {
		this.displayName = cache.users[this.sid].playerName;
		this.personaState = cache.users[this.sid].personaState;
	}
};

User.prototype.unset = function(name) {
	delete this.props[name];
};

User.prototype.get = function(name) {
	if (props[name].display != undefined) {
		return props[name].display(this.props[name]) || props[name].defaultValue;
	}
	return this.props[name] || props[name].defaultValue;
};

User.prototype.display = function(name) {
	if (props[name].display != undefined) {
		return props[name].display(this.props[name]) || props[name].defaultValue;
	}
	return this.props[name] || props[name].defaultValue;
};

User.prototype.set = function(name, val, type = 'user') {
	if (levels[this.props[name].type] > levels[type]) {
		return 'You do not have permission to do that!';
	}

	try {
		this.props[name] = props[name].save(val);
	} catch (e) {
		return e;
	}

	return null;
};

User.prototype.save = function() {
	// Get the user object
	var user = users[this.sid] || {};

	// Save it
	users[this.sid] = user.props;
	fs.writeFile('users.json', JSON.stringify(users, null, '\t'));
};

// Create a bot and set up error handling
var bot = new Steam.SteamClient();
bot.on('error', function(e) {
	console.log(e);
	process.exit(1);
});

// Try to log in
bot.logOn({
	accountName: settings.accountName,
	password: settings.password
});

// Handle when we get a successful login
bot.on('loggedOn', function() {
	console.log('Logged in!');
	bot.setPersonaState(Steam.EPersonaState.Online);
	bot.setPersonaName(settings.displayName);
});

bot.on('servers', function(servers) {
	cache.servers = servers;
	fs.writeFile('cache.json', JSON.stringify(cache, null, '\t'));
});

// Display when users sign on or off
// TODO: Make signoff notifications work
bot.on('user', function(user) {
	if (user.personaState == Steam.EPersonaState.Online) {
		console.log(user.playerName + ' just came online.');
	} else if (user.personaState == Steam.EPersonaState.Offline) {
		console.log(user.playerName + ' just logged off.');
	}
	cache.users[user.friendid] = user;
});

// Handle any friend invites we get while we're online
bot.on('friend', function(sid, friendStatus) {
	// TODO: Maybe display the name of who added us?
	// TODO: Display when someone unfriends us?
	if (friendStatus == Steam.EFriendRelationship.PendingInvitee) {
		bot.addFriend(sid);
		bot.sendMessage(sid, settings.welcomeText);
	}
});

// Handle any friend invites we got while offline
bot.on('relationships', function() {
	// TODO: Maybe display the name of who added us?

	// Loop through our 'friends' and accept any invites.
	for (var sid in bot.friends) {
		if (bot.friends[sid] == Steam.EFriendRelationship.PendingInvitee) {
			bot.addFriend(sid);
			bot.sendMessage(sid, settings.welcomeText);
		}
	}
});

bot.on('message', function(source, message, type, chatter) {
	if (type != Steam.EChatEntryType.ChatMsg) {
		// We only respond to chat messages
		return;
	}

	// Look up our user
	var s = new User(source);

	// Parse the data out of the message
	var data = cmdCheck.exec(message);

	// If there wasn't a match, send them the welcomeText
	if (data == null) {
		bot.sendMessage(source, settings.welcomeText);
		return;
	}

	var cmd = data[1];
	var text = data[2];

	try {
		switch (cmd) {
			case 'ping':
				bot.sendMessage(source, 'pong');
				break;
			case 'sid':
				bot.sendMessage(source, source);
				break;
			case 'seed':
				if (!(s.get('seeder') || s.get('admin'))) {
					bot.sendMessage(source, 'You don\'t have permission to do that!');
					return;
				}

				var msgPrefix = '';
				if (s.displayName != undefined) {
					msgPrefix = s.displayName + ': ';
				}

				var count = 0;
				for (var sid in bot.friends) {
					var user = new User(sid);

					// Check user exclusions
					if (user.friendStatus != Steam.EFriendRelationship.Friend) {
						// Only message friends
						continue;
					} else if (sid == source) {
						// Don't send to the seeder
						continue;
					} else if (sid == bot.steamID) {
						// Don't message ourself
						continue;
					} else if (user.personaState != Steam.EPersonaState.Online) {
						// TODO: Fix notify_if_away
						if (user.personaState == Steam.EPersonaState.Away &&
								user.notify_if_away) {
									// If they're away and still want to be notified, notify them
								} else if (user.personaState == Steam.EPersonaState.Busy &&
										user.notify_if_busy) {
											// If they're busy and still want to be notified, notify them
										} else {
											continue;
										}
					}

					// TODO: Setting for only sending if not in game
					// TODO: Exclusion options
					// * Send if in game other than TF2
					// * Send only if in TF2
					// * Don't send if on certain IP
					// * Don't send if playing on the Ville.

					bot.sendMessage(source, "Sending message to " + user.playerName);
					bot.sendMessage(sid, msgPrefix + text);
					count++;
				}

				bot.sendMessage(source, "" + count + " messages sent.")
					break;
			case 'unsetuser':
				if (!s.admin) {
					bot.sendMessage(source, 'You don\'t have permission to do that!');
					return;
				}
				var args = splitText(text);
				if (args.length != 2 || user_props.indexOf(args[0]) == -1) {
					bot.sendMessage(source, 'Usage: !unsetuser userid setting');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}

				var user = new User(args[0]);
				if (user == null) {
					bot.sendMessage(source, 'That is not a valid Steam ID.');
				}

				text = args[1];
			case 'unset':
				var args = splitText(text);
				if (args.length != 1 || props[args[0]] == undefined) {
					bot.sendMessage(source, 'Usage: !unset setting');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}

				var user = user || s;
				user.unset(args[0]);
				user.save();

				break;
			case 'setuser':
				if (!s.admin) {
					bot.sendMessage(source, 'You don\'t have permission to do that!');
					return;
				}
				var args = splitText(text);
				if (args.length != 3 || props[args[0]] == undefined) {
					bot.sendMessage(source, 'Usage: !setuser userid setting value');
					bot.sendMessage(source, 'Value is optional. The assumed value is true.');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}
				var user = new User(args.shift());
				if (user == null) {
					bot.sendMessage(source, 'That is not a valid Steam ID.');
				}
				text = args.join(' ');
			case 'set':
				var args = splitText(text);
				if (args.length != 2 || user_props.indexOf(args[0]) == -1) {
					bot.sendMessage(source, 'Usage: !set setting value');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}

				var user = user || s;
				user.set(args[0], args[1]);
				user.save();

				bot.sendMessage(source, 'Setting ' + args[0] + ' to ' + val + '.');

				break;
			case 'getuser':
				if (!s.admin) {
					bot.sendMessage(source, 'You don\'t have permission to do that!');
					return;
				}
				var args = splitText(text);
				if (args.length != 2 || user_props.indexOf(args[0]) == -1) {
					bot.sendMessage(source, 'Usage: !getuser userid setting');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}
				var user = new User(args[0]);
				text = args[1];
			case 'get':
				var args = splitText(text);
				if (args.length < 1 || user_props.indexOf(args[0]) == -1) {
					bot.sendMessage(source, 'Usage: !get setting');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('user').join(', '));
					return;
				}

				var user = user || s;
				bot.sendMessage(source, 'Setting ' + args[0] + ' is ' + user[args[0]]);

				break;
			case 'prop':
				if (!s.admin) {
					bot.sendMessage(source, 'You don\'t have permission to do that!');
					return;
				}
				var args = splitText(text);
				if (args.length < 2 || args.length > 3 || admin_props.indexOf(args[1]) == -1) {
					bot.sendMessage(source, 'Usage: !prop userid property [value]');
					bot.sendMessage(source, 'If no value is included, the current value will be displayed.');
					bot.sendMessage(source, 'Possible settings: ' + propsFor('admin').join(', '));
					return;
				}

				var user = new User(args[0]);
				var val = args[2];
				if (val == undefined) {
					bot.sendMessage(source, 'Setting ' + args[1] + ' is ' + user[args[1]]);
					return;
				}

				user.set(args[1], val, 'admin');
				user.save();

				bot.sendMessage(source, 'Setting ' + args[1] + ' to ' + val + '.');
				break;
			default:
				bot.sendMessage(source, 'Unknown command.');
				console.log('cmd: ' + cmd);
				console.log('text: ' + text);
				break;
		}
	} catch (e) {

	}
});
