# props.coffee

cleanBool = (val) ->
	switch val.toLowerCase()
		when "true", "t", "yes", "y", "on", "1" then return true
		when "false", "f", "no", "n", "off", "0" then return false
		else throw "That is not a valid value"

props =
	admin:
		default: false
		clean: cleanBool

	seeder:
		default: false
		clean: cleanBool

	notify_when:
		default: [
			'online'
			'in_game'
		]
		
		# TODO: Add 'offline' to force send
		values: [
			'online'
			'away'
			'busy'
			'in_game'
		]
		clean: (val) ->
			vals = val.split ','
			for type in vals
				if not type in @values
					help = ', '.join @values
					throw "#{type} is not a valid value (#{help})"
			return vals

	app_list_type:
		default: 'off'
		value: [
			'off'
			'blacklist'
			'whitelist'
		]
		clean: (val) ->
			if not val in @value
				throw "That is not a valid value."
			return val

	app_list:
		default: []
		clean: (val) ->
			vals = val.split ','
			ret = []
			for num in vals
				n = parseInt(num, 10)
				if n is NaN
					throw "#{num} is not a valid app id."
				ret.push n
			return ret

exports.exists = (prop) ->
	return props[prop]?

exports.clean = (prop, val) ->
	return props[prop]?.clean?(val) ? val

exports.default = (prop) ->
	return props[prop]?.default
