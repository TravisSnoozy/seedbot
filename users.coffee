# user.coffee

# std packages
fs = require 'fs'

# non-std packages
Steam = require 'steam'
ip = require 'ip'

# local packages
prop = require './prop.coffee'
steamid = require './steamid.coffee'

adminProps = [
	'admin'
	'seeder'
]

userProps = {}
if fs.existsSync 'users.json'
	userProps = JSON.parse fs.readFileSync 'users.json'

class exports.User
	constructor: (sid, @bot) ->
		@sid = steamid.normalize(sid)
		@displayName = @bot.users[@sid]?.playerName
		@personaState = @bot.users[@sid]?.personaState
		@gameId = @bot.users[@sid]?.gamePlayedAppId
		@gameIp = ip.fromLong @bot.users[@sid]?.gameServerIp
		@props = userProps[@sid] ? {}
		@friendStatus = @bot.friends[@sid] ? Steam.EFriendRelationship.None

	get: (key, user = null) ->
		if not prop.exists(key)
			throw "That property doesn't exist"

		if user? and not user.get('admin')
			throw "You don't have permission to do that"

		return @props[key] ? prop.default(key)

	set: (key, val, user = null) ->
		if not prop.exists(key)
			throw "That property doesn't exist"

		if user? and not user.get('admin')
			throw "You don't have permission to do that"

		if not user? and key in adminProps and not @get('admin')
			throw "You don't have permission to do that"

		@props[key] = prop.clean(key, val)
		@save()

	unset: (key, user = null) ->
		if not prop.exists(key)
			throw "That property doesn't exist"

		if user? and not user.get('admin')
			throw "You don't have permission to do that"

		delete @props[key]
		@save()

	save: ->
		userProps[@sid] = @props
		fs.writeFile 'users.json', JSON.stringify(userProps, null, '\t')

