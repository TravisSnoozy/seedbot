# Ville Seedbot

## The meaning of SteamID in this package

## Commands

* get
* set
* unset

## Admin commands

* setuser
* seed
* seedip

## Initial config

### settings.json

* `accountName`: Account name of the bot
* `displayName`: Display name of the bot
* `password`: Password of the bot
* `ipReplacements`: Object with names as keys and replacements as values

### users.json

Users are stored in users.json. A single user must be added with admin for this to work.

The file is structured as follows:

```json
{
	"steam community id": {
		"admin": true
	}
}
```

## Settings

This is a list of all properties and how they are configured.

### admin

`admin` is simply a boolean property.

It can only be set by other admins.

### seeder

`seeder` is simply a boolean property.

It can only be set by admins.

### notify_when

`notify_when` is a comma-separated list of properties.

The possible values are:

* online
* in_game
* away
* busy

### app_list_type

`app_list_type` is one of the following properties. It is used to determine if you are `in_game`.

* off: The `app_list` will not be used. If you are in any game, and have `in_game` set you will get messages.
* blacklist: `app_list` acts as a blacklist. If you are playing a game and in one of the apps listed, you will not get a message. If you are in anything else, you will.
* whitelist: `app_list` acts as a whitelist. If you are playing a game and in one of the apps listed, you will get a message. If you are in anything else, you will not.

### app_list

`app_list` is a comma-separated list of Steam App IDs. How they are used depends on the `app_list_type` property.
